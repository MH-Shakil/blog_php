<?php 
session_start();
if(!isset($_SESSION['isAdmin']))
{
  header("location:../page-login.php");
}
include "../db/connection.php";
/* admin header*/
require_once("pages/admin-header.php");
require_once("pages/admin-top-header.php");
require_once("pages/admin-menu.php");

$u_select="SELECT * FROM users";
$u_result = $con->query($u_select);
$u_rows = mysqli_num_rows($u_result);

$c_select="SELECT * FROM categories";
$c_result = $con->query($c_select);
$c_rows = mysqli_num_rows($c_result);

$p_select="SELECT * FROM posts";
$p_result = $con->query($p_select);
$p_rows = mysqli_num_rows($p_result);



?>
<div id="layoutSidenav_content">
<main>
  <div class="container-fluid">
    <h1 class="mt-4">Dashboard</h1>
    <ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">Dashboard</li>
    </ol>
  <div class="card mb-4">
      <div class="card-header">
        <i class="fas fa-table mr-1"></i>
           All user table
      </div>
  </div>
</main>
<div class="row row-cols-1 row-cols-md-4 container">
  <?php
    $select="SELECT * FROM users ORDER BY id DESC";
          $result=$con->query($select);

          $html = '';
          foreach ($result as $key => $value) {
            $html.='<div class="col mb-3 shadow p-3 mb-5 bg-white rounded border">
                    <div class="card h-100">
                      <img style="height:245px" src="../upload/userImage/'.$value['image'].'" class="card-img-top" alt="...">
                     <div class="card-body">
                        <h5 class="card-title">'.$value['name'].'</h5>
                        <h6 class="card-title">'.$value['id'].'</h6>
                        <h6 class="card-title">'.$value['email'].'</h6>
                        <h6 class="card-title">'.$value['password'].'</h6>
                        <h6 class="card-title">'.$value['created_at'].'</h6>
                            <div class="">
                             <a class="btn btn-info"  href="#?id='.$value['id'].'">Viwe</a>
                              <a class="btn btn-warning"  href="user-update.php?id='.$value['id'].'">Edit</a>
                              <a class="btn btn-danger"  href="user-delete.php?id='.$value['id'].'">Delete</a>

                            </div>
                      </div>
                    </div>
                  </div>';
                }
                echo $html;

  ?>
  
</div>


<?php
require_once("pages/admin-footer.php");
?>