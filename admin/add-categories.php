<?php 
/*start session and check*/
session_start();
if(!isset($_SESSION['isAdmin']))
{
  header("location:../page-login.php");
}
/*end session check*/

/*start admin needed page include*/
include "../db/connection.php";
require_once("pages/admin-header.php");
require_once("pages/admin-top-header.php");
require_once("pages/admin-menu.php");
/*start admin needed page include*/

  /*start insert categories code start */
  if(isset($_POST['categories']))  { 
 //if categories form value found than this code is work 
  $categories=$_POST['categories'];
  $insert="INSERT INTO categories(name) VALUES('$categories')";
  $insert_data=$con->query($insert); //check categories insert categories query
  if (!$insert_data) {
    echo 'data not inserted';
  }
}
/*end insert categories code start */


?>


        
<div id="layoutSidenav_content">
  <main>
    <div class="container-fluid">
      <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4">
          <li class="breadcrumb-item active">Dashboard</li>
        </ol>
        <div class="row">

          <!-- start categoris input form -->
          <div class="col-md-5">
            <form action="" method="POST" id="form-submit">
              <label class="form-label"><h5>Add Categories</h5></label>
              <input id="categories" type="text" name="categories" class="form-control mb-2">
              <button type="submit" class="btn btn-info">Submit</button>
            </form>
          </div>
          <!-- end categoris input form -->

          <!-- start show categories table -->
          <div class="col-md-7">
            <h2 style="">CATEGORIES NAME</h2>
            <table class="table" style="">
  
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Created_at</th>
                  <th>ACTION</th>
                </tr>
              </thead>

              <tbody>
                <?php 
                $select="SELECT * FROM categories  ORDER BY id DESC";
                $result=$con->query($select);

              $html = '';
                foreach ($result as $key => $value) {
                  $html.='<tr>
                            <td>'.$value['id'].' </td>
                            <td>'.$value['name'].' </td>
                            <td>'.$value['created_at'].' </td>
                            
            <td><a class="btn btn-info"  href="viweStd.php?id='.$value['id'].'">Viwe</a>
           ';
          }

        echo $html;

    ?>
          <tbody>
        </table>
        </div>
        <!-- end show categories table -->
                         
      </div>
    </div>
  </main>
                    


    <?php
    /*added page footer */
    require_once("pages/admin-footer.php");
    ?>