<?php 
session_start();

if(!isset($_SESSION['isAdmin']))
{
  header("location:../page-login.php");
}

include "../db/connection.php";

/* admin header*/
require_once("pages/admin-header.php");
require_once("pages/admin-top-header.php");
require_once("pages/admin-menu.php");
/*session_start();
include "navbar.php";
$tag_id=$_GET['tag_id'];*/
  $id=$_GET['id'];
  $select="SELECT * FROM categories WHERE id=$id";
  $select_query=$con->query($select);
  foreach ($select_query as $key => $value) {
   

?>


        
<div id="layoutSidenav_content">
  <main>
    <div class="container-fluid">
      <h1 class="mt-4">Edit-Category</h1>
        <ol class="breadcrumb mb-4">
          <li class="breadcrumb-item active">Category Update</li>
        </ol>
        <div class="row">
          <div class="col-md-5">
            <form action="all-update.php" method="POST" >
              <input 
                type="hidden" 
                name="id" 
                value="<?= $_GET['id']?>">

                <label class="form-label">
                  <h5>Update Category </h5>
                </label>

                <input 
                  id="categories" 
                  type="text" 
                  name="categories" 
                  class="form-control mb-2" 
                  value="<?= $value['name']?>" />

                <button 
                    type="submit" 
                    class="btn btn-info" 
                    name="cat_update">
                  Update
                </button>
            </form>
          </div>                           
      </div>
    </div>
  </main>
                    


    <?php

     }
     /*data show end*/
    require_once("pages/admin-footer.php");
    ?>