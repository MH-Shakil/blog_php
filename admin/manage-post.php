<?php 
  session_start();
  if (!isset($_SESSION['isAdmin'])) {
    header('location:../page-login.php');
  }
  include '../db/connection.php';
  include 'pages/admin-header.php';
  include 'pages/admin-top-header.php';
  include 'pages/admin-menu.php';

  /* Post Aproval start */
  if (isset($_GET['status']) && isset($_GET['id'])) {
    $status = $_GET['status'];
    $id = $_GET['id'];
    
    $update = "UPDATE posts SET status='$status' WHERE id=".$id;

  }


  /* Post Aproval End */



  ?>
    <div id="layoutSidenav_content">
  <main>
    <div class="container-fluid">
      <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4"  style="text-align: center;">
          <li class="breadcrumb-item active">ALL CATEGORIES </li>
        </ol>
        <div class="row">
          <!-- <div class="col-md-5">
            <form action="" method="POST" id="form-submit">
              <label class="form-label"><h5>Add Categories</h5></label>
              <input id="categories" type="text" name="categories" class="form-control mb-2">
              <button type="submit" class="btn btn-info">Submit</button>
            </form>
          </div> -->
          <div class="col-md-12">
            
            <table class="table" style="" border="1" >
          <!-- table use for table -->
              <thead style="text-align: center;">
                <tr>
                  <th>ID</th>
                  <th>TITLE</th>
                  <th>DESCRIPTION</th>
                  <th>IMAGE</th>
                  <th>POSTER ID</th>
                  <th>STATUS</th>
                  <th>CATEGORY ID</th>
                  <th>Created_at</th>
                  <th>ACTION</th>
                </tr>
              </thead>
              <tbody>

  <?php
  $p_seletc="SELECT * FROM posts ORDER BY id DESC";
  $p_query=$con->query($p_seletc);
  $html='';
  foreach ($p_query as $key => $p_value) { 
      $isEnable = $p_value['status'] == 1 ? 'Enable' : 'Disable';
      $post=$p_value['description'];
      $poststr=substr($post,0,200);
      $html.='<tr>
                            <td>'.$p_value['id'].' </td>
                            <td>'.$p_value['title'].' </td>
                            <td>'.$poststr.' </td>
                            <td><img height="140px" width="180px" src="../upload/'.$p_value['image'].'"></td>
                            <td>'.$p_value['poster_id'].' </td>
                            <td>'.$isEnable.' </td>
                            <td>'.$p_value['category_id'].' </td>
                            <td>'.$p_value['created_at'].' </td>
                           <td> 
            <a class="btn btn-info" href="view-post.php?id='.$p_value['id'].'">View</a> 
            <a class="btn btn-danger mt-1"   onclick="return confirm(\'Are you sure delete this post ?\')" href="delete-post.php?id='.$p_value['id'].'">Delete</a> 
            <a class="btn btn-success mt-1"   onclick="return confirm(\'Are you sure Enable or Disable this post?\')" href="aprove.php?id='.$p_value['id'].'&status='.$p_value['status'].'">
              Aprove
            </a> 
            </td>';
          }

        echo $html;

 ?>
         </table>
        </div>
                              
      </div>
    </div>
  </main>
                    


    <?php
    require_once("pages/admin-footer.php");
    ?>
