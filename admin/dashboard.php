<?php 
/*start and check session */
session_start();
if(!isset($_SESSION['isAdmin']))
{
  header("location:../page-login.php");
}
/*end check session*/

/*start admin needed page include*/

include "../db/connection.php";
require_once("pages/admin-header.php");
require_once("pages/admin-top-header.php");
require_once("pages/admin-menu.php");

/*start admin needed page include*/

/*start user select for count total amount of users*/
$u_select="SELECT * FROM users";
$u_result = $con->query($u_select);
$u_rows = mysqli_num_rows($u_result);
/*end user select for count total amount of users*/

/*start categories select for count total categories*/
$c_select="SELECT * FROM categories";
$c_result = $con->query($c_select);
$c_rows = mysqli_num_rows($c_result);
/*end categories select for count total categories*/

/*start post select for count total amount of posts*/
$p_select="SELECT * FROM posts";
$p_result = $con->query($p_select);
$p_rows = mysqli_num_rows($p_result);
/*end post select for count total amount of posts*/



?>


<!-- start show total count users,categories,visitor and posts -->
<div id="layoutSidenav_content">
<main>
  <div class="container-fluid">
    <h1 class="mt-4">Dashboard</h1>
    <ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">Dashboard</li>
    </ol>
  <div class="row">
    <div class="col-xl-3 col-md-6">
      <div class="card bg-primary text-white mb-4">
        <div class="card-body"><h5>Total Mamber <?php echo $u_rows;?></h5></div>
        <div class="card-footer d-flex align-items-center justify-content-between">
            <a class="small text-white stretched-link" href="users-details.php">View Mamber</a>
            <div class="small text-white"><i class="fas fa-angle-right"></i></div>
        </div>
    </div>
  </div>
  <div class="col-xl-3 col-md-6">
    <div class="card bg-warning text-white mb-4">
        <div class="card-body"><h5>Total Categories <?php echo $c_rows;?></h5></div>
        <div class="card-footer d-flex align-items-center justify-content-between">
            <a class="small text-white stretched-link" href="manage-categories.php">View Details</a>
            <div class="small text-white"><i class="fas fa-angle-right"></i></div>
        </div>
    </div>
  </div>
  <div class="col-xl-3 col-md-6">
    <div class="card bg-success text-white mb-4">
        <div class="card-body"><h5>Total Visitor</h5></div>
        <div class="card-footer d-flex align-items-center justify-content-between">
            <a class="small text-white stretched-link" href="#">View Details</a>
            <div class="small text-white"><i class="fas fa-angle-right"></i></div>
        </div>
    </div>
  </div>
  <div class="col-xl-3 col-md-6">
    <div class="card bg-danger text-white mb-4">
        <div class="card-body"><h5>Total Post <?php echo $p_rows;?></h5></div>
        <div class="card-footer d-flex align-items-center justify-content-between">
            <a class="small text-white stretched-link" href="manage-post.php">View Details</a>
            <div class="small text-white"><i class="fas fa-angle-right"></i></div>
        </div>
    </div>
  </div>
</div>
<!-- end total count users,categories,visitor and posts -->

<!-- start chart example -->
<div class="row">
  <div class="col-xl-6">
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-chart-area mr-1"></i>
            Area Chart Example
        </div>
        <div class="card-body"><canvas id="myAreaChart" width="100%" height="40"></canvas></div>
    </div>
  </div>
<div class="col-xl-6">
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-chart-bar mr-1"></i>
            Bar Chart Example
        </div>
        <div class="card-body"><canvas id="myBarChart" width="100%" height="40"></canvas></div>
    </div>
  </div>
</div>
<!-- end chart example -->

<!-- start categories show part -->
  <div class="card mb-4">
      <div class="card-header">
        <i class="fas fa-table mr-1"></i>
           DataTable Example
      </div>
  <div class="card-body">
    <div class="table-responsive">
      <table 
        class="table" 
        id="dataTable" 
        width="100%" 
        cellspacing="0">
        
          <thead>
            <tr>
              <th> ID </th>
              <th> Name </th>
              <th> created_at </th>
              <th> ACTION </th>
            </tr>
          </thead>

          <tbody>
          <?php 
          $select="SELECT * FROM categories";
          $result=$con->query($select);

          $html = '';
          foreach ($result as $key => $value) {
            $html.='<tr>
                      <td>'.$value['id'].' </td>
                      <td>'.$value['name'].' </td>
                      <td>'.$value['created_at'].' </td>
                      
          <td><a class="btn btn-info"  href="#?id='.$value['id'].'">Viwe</a>';
          }

          echo $html;

          ?>
          </table>
                    
        </div>
      </div>
    </div>
    <!-- end categories show part -->
  </div>
</main>



<?php

/*added footer page*/
require_once("pages/admin-footer.php");
/*added footer page*/

?>