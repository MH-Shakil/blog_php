<?php
session_start();

if(!isset($_SESSION['isAdmin']))
{
  header("location:../page-login.php");
}
include "../db/connection.php";

/* admin header*/
require_once("pages/admin-header.php");
require_once("pages/admin-top-header.php");
require_once("pages/admin-menu.php");


/*
  1.  !empty($_GET['id']) 
    Inisde : etar mane holo id shoman jodi kono value na thake tahole takke redirect kore dite hobe. 
  2. isset($_GET['id']) etar mane jodi url a user a user-update.php?id emon thake
*/ 





?>

<div id="layoutSidenav_content">
<main>
  <div class="container-fluid">
    <h1 class="mt-4">Dashboard</h1>
    <ol class="breadcrumb mb-4">
    <li class="breadcrumb-item active">Dashboard</li>
    </ol>
  
<?php 



if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = $_GET['id'];
    $u_select="SELECT * FROM users where id='$id'";
    $u_result = $con->query($u_select);
    $u_rows = mysqli_num_rows($u_result);

    foreach ($u_result as $key => $row) {
    ?>  


    
<div class="row">
  <div class="col-md-2"></div>

  <div class="col-md-9">
    <form action="all-update.php" method="POST">

      <div class="mb-3">
        <label class="form-label">USER NAME</label>
        <input class="form-control" type="text" name="name" class="form-control" value="<?php echo $row['name']?>">
      </div>

      <input type="hidden" name="id" value="<?= $_GET['id'] ?>">

      <div class="mb-3">
        <label class="form-label">EMAIL</label>
        <input type="text" name="email" class="form-control" value="<?php echo $row['email']?>">
      </div>

      <div class="mb-3">
        <label class="form-label">PASSWORD</label>
        <input type="text" name="password" class="form-control" value="<?php echo $row['password']?>">
      </div>

      <div class="mb-3">
        <label class="form-label">isAdmin</label>
        <input type="number" name="isAdmin" class="form-control" value="<?php echo $row['isAdmin']?>">
      </div>

        <div class="col-12">
            <input class="btn btn-primary" type="submit" name="user_update" value="Update">
        </div>

    </form>
  </div>
</div>


<?php 
  }
}else{
  header("location:dashboard.php");
}
?>

<?php
  require_once("pages/admin-footer.php");
?>