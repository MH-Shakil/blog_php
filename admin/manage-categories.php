<?php 
session_start();

include "../db/connection.php";

/*if(!isset($_SESSION['isAdmin']))
{
  header("location:../page-login.php");
}*/

/* admin header*/
require_once("pages/admin-header.php");
require_once("pages/admin-top-header.php");
require_once("pages/admin-menu.php");


?>


        
<div id="layoutSidenav_content" class="">
  <main>
    <div class="container-fluid ">
      <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4"  style="text-align: center;">
          <li class="breadcrumb-item active">ALL CATEGORIES </li>
        </ol>
        <div class="row">
          <!-- <div class="col-md-5">
            <form action="" method="POST" id="form-submit">
              <label class="form-label"><h5>Add Categories</h5></label>
              <input id="categories" type="text" name="categories" class="form-control mb-2">
              <button type="submit" class="btn btn-info">Submit</button>
            </form>
          </div> -->
          <div class="col-md-12">
            
            <table class="table" style="">
          <!-- table use for table -->
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Created_at</th>
                  <th>ACTION</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $select="SELECT * FROM categories  ORDER BY id DESC";
                $result=$con->query($select);

              $html = '';
                foreach ($result as $key => $value) {
                  $html.='<tr>
                            <td>'.$value['id'].' </td>
                            <td>'.$value['name'].' </td>
                            <td>'.$value['created_at'].' </td>
                            
            <td><a class="btn btn-info"  href="viweStd.php?id='.$value['id'].'">Viwe</a>

           <a class="btn btn-warning" href="edit-categories.php?id='.$value['id'].'">Edit</a> 

            <a class="btn btn-danger"   onclick="return confirm(\'Are you sure ?\')" href="cat-delete.php?id='.$value['id'].'">Delete</a> ';
          }

        echo $html;

    ?>
        </table>
        </div>
                              
      </div>
    </div>
  </main>
                    


    <?php
    require_once("pages/admin-footer.php");
    ?>