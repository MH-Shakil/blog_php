<?php
session_start();
  include 'db/connection.php';
  include "pages/header.php";
  include "pages/nav.php";

 ?>
  <!-- Page Content -->
  <div class="container">

    <div class="row">

      <div class="col-md-8">
         <?php include "left-user-post.php";?>
      </div>

      <div class="col-md-4">
        <?php include "post-right.php";?> 
      </div>
     
    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

 <?php
  include "pages/footer.php";
 ?>
