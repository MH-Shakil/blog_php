
<div class="">
  <h5 class="card-header" style="">Write a post</h5>
  <div class="card-body">
    <form method="POST" action=""  enctype="multipart/form-data">
          <div class="form-group">
            <div class="row">
              <div class="col-md-1"><h5 style="margin-top: 5px;">Title:</h5></div>
              <div class="col-md-11">
                 <textarea type="text" name="title" style="border:none;" class="form-control" rows="1" placeholder="Write a page title..."></textarea> 
              </div>
            </div>
          </div>
      
      <div class="form-group">
        <textarea name="description" class="form-control" rows="3" placeholder="Write post here..."></textarea>
      </div>
         <div class="form-group">
         <select name="categories" class="btn mr-1 btn-info">
      <?php 
              $select="SELECT * FROM categories";
              $query=$con->query($select);
              foreach ($query as $key => $rows) {
              echo '<option value='.$rows['id'].'>'.$rows['name'].'</option>';

              }

        ?>
        </select>
         <input class="btn btn-outline-dark" type="file" name="file">
      </div>
      <input type="submit" class="btn btn-primary" name="submit" placeholder="Post">
    </form>
  </div>
</div>
