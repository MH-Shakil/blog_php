      <!-- Sidebar Widgets Column -->


        <!-- Search Widget -->
        <div class="card my-4">
          <h5 class="card-header">Search</h5>
          <div class="card-body">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search for...">
              <span class="input-group-append">
                <button class="btn btn-secondary" type="button">Go!</button>
              </span>
            </div>
          </div>
        </div>

        <!-- Categories Widget -->
        <div class="card my-4">
          <h5 class="card-header">Categories</h5>
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <?php 
                  $c_select="SELECT * FROM Categories ORDER BY id DESC";
                  $c_query=$con->query($c_select);
                  $html='';
                  foreach ($c_query as $key => $c_value) {
                    $html.='
                                  <a class="btn btn-info "style="margin:3px;" href="category-post.php?id='.$c_value['id'].'">'.$c_value['name'].'</a>
                                ';
                  }
                  echo $html;
                 ?>
          
              </div>
            </div>
          </div>
        </div>

        <!-- Side Widget -->
        <div class="card my-4">
          <h5 class="card-header">Side Widget</h5>
          <div class="card-body">
            You can put anything you want inside of these side widgets. They are easy to use, and feature the new Bootstrap 4 card containers!
          </div>
        </div>
