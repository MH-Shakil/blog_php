<?php
  ob_start();
  session_start();
  if (!isset($_SESSION['id'])) {
    header('location:index.php');
  }
   include 'db/connection.php';
   include 'pages/header.php';
   require_once('pages/nav.php');

   $r_id=$_GET['id'];
   $s_id=$_SESSION['id'];
   $month=date('M'); 
   $day=date('d');
   $date=$month.' '.$day; 
    if ($_SERVER['REQUEST_METHOD']=='POST') {
    $text=$_POST['text'];
    $m_insert = "INSERT INTO message(description,sender_id,receiver_id,created_at)VALUES('$text','$s_id',".$r_id.",'$date')";
    $m_query=$con->query($m_insert);

    if ($m_query) {
      header('Location:chat.php?id='.$r_id);
    }else{
    }
  }

 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>chat list</title>
  <link rel="stylesheet" type="text/css" href="pages/chat.css">
</head>
<body>
  <div id="chat-container">
      <div id="search-container">
          <input type="tex" name="" placeholder="Search...">
      </div>
      <!-- start php code for slect cnversation list -->
      <div id="conversation-list">

      <?php
          $id=$_SESSION['id'];
          $select="SELECT * FROM message WHERE sender_id='$id' OR receiver_id='$id'";
          $list_array=[];
          $query=$con->query($select);
          foreach ($query as $key => $value)
          {
            if ($value['sender_id'] == $id) 
            {
                if (in_array($value['receiver_id'], $list_array) !=true) {
                  array_push($list_array, $value['receiver_id']);
                }
            }
            else
            {
              if (in_array($value['sender_id'], $list_array) !=true) {
                array_push($list_array, $value['sender_id']);
              }
            }
          }

        /* Fetch users data from users whom i made conversation start using link*/
        foreach ($list_array as $key => $value) {
          $user = $con->query("SELECT * FROM users where id='$value'");
          foreach ($user as $i => $info) {
        
        

      
      echo '<a href="chat.php?id='.$info['id'].'" style="text-decoration:none;">
            <div class="conversation">
              <img src="upload/userImage/'.$info['image'].'">
              <div class="title-text">
                '.$info['name'].'
              </div>
              <div class="created-date">
                 Apr 6
              </div>
              <div class="conversation-message">
               This is a messsage 
             </div>
            </div> 
          </a>';
    
     }
    }
  ?>
      </div>
      <!-- end conversation list -->

      <div id="new-message-container">
          <a href="#">+</a>        
      </div>


      <div id="chat-title">
        <?php 
          $select="SELECT * FROM users WHERE id=".$_GET['id'];

          $query=$con->query($select);
          foreach ($query as $key => $info) {
            echo '<span> <img src="upload/userImage/'.$info['image'].'" class="mr-2">'.$info['name'].'</span>
          <a href="delete_conversation.php?id='. $r_id.'"><img src="upload/delete.png" alt=""></a>';
          }
        ?> 
      </div>

      <!-- start fatch message -->
      <div id="chat-message-list">
        <?php
        $m_select="SELECT * FROM message WHERE sender_id='$s_id' AND receiver_id='$r_id' OR sender_id='$r_id' AND receiver_id='$s_id' ORDER BY id DESC";
      $m_data=$con->query($m_select);

      foreach ($m_data as $key => $m_value) {
      if ($m_value['sender_id']==$_SESSION['id']) {

          echo '<div class="message-row you-message">
              <div class="message-content">
                  <div class="message-text">'.$m_value['description'].'</div>
                  <div class="message-time">'.$m_value['created_at'].'</div>
              </div>
          </div>';

        }else{
         echo '<div class="message-row other-message">
              <div class="message-content">
                  <img src="upload/2.jpg" alt="..">
                  <div class="message-text">
                      '.$m_value['description'].' 
                  </div>
                  <div class="message-time">Apr 6</div>
              </div>
          </div>';
      }
    }?>
          
          
          
      </div>
      <form method="POST" action="">
      <div id="chat-form">
         <img src="upload/attachment-logo.png"
              alt="..."
              style="margin-top: 20px;">
          <input type="text"
              name="text" 
              placeholder="Type a message">
          <button class="btn btn-info" name="submit" type="submit" 
              style="margin-bottom: 4px; padding: 5px 15px; font-size: 16px;
              font-weight: bold">
            Send
          </button>
          
      </div>
    </form>
  </div>
</body>
</html>