<?php
session_start();
include 'db/connection.php';
include 'pages/header.php';
include 'pages/nav.php';
$poster_id=$_SESSION['id'];
$id=$_GET['id'];
$p_select="SELECT * FROM posts WHERE id=".$id;
$p_query= $con->query($p_select);
foreach ($p_query as $key => $p_value) {
?>
<div class="container">
<div class="row">
  <h5 class="card-header" style="">Update post</h5>
  <div class="card-body">
    <form method="POST"  enctype="multipart/form-data">
      <input type="hidden" name="id" value="<?php echo $p_value['id']?>">
      <div class="form-group">
        <img height="400px" width="800px" src="upload/<?= $p_value['image']?>">
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-md-1"><h5 style="margin-top: 5px;">Title:</h5></div>
          <div class="col-md-11">
             <textarea type="text" name="title" style="border:none;" class="form-control" rows="1"><?php echo $p_value['title'];?></textarea> 
          </div>
        </div>
      </div>
      
      <div class="form-group">
        <textarea name="description" class="form-control" rows="3" ><?php echo $p_value['description'];?></textarea>
      </div>
         <div class="form-group">
         <select name="categories" class="btn mr-1 btn-info">
      <?php 
              $select="SELECT * FROM categories";
              $query=$con->query($select);
              foreach ($query as $key => $rows)
               {
                if ($rows['id'] == $p_value['category_id']) {
                 
                echo '<option value='.$rows['id'].' selected >'.$rows['name'].'</option>';

                }
                echo '<option value='.$rows['id'].'  >'.$rows['name'].'</option>';
                }

        ?>
        </select>
         <input class="btn btn-outline-dark" type="file" name="file">
      </div>
      <input type="submit" class="btn btn-primary" name="submit" placeholder="Update">
    </form>
  </div>
</div>
<?php } ?>

<?php
if (isset($_POST['submit']) && isset($_POST['id'])){
  $poster_id=$_POST['id'];
  $title=$_POST['title'];
  $description=$_POST['description'];
  $categories=$_POST['categories'];
  $file_name = $_FILES['file']['name'];
  $file_type = $_FILES['file']['type'];
  $file_size = $_FILES['file']['size'];
  $file_tmp_loc = $_FILES['file']['tmp_name'];
  $file_store = 'upload/'.$file_name;
  move_uploaded_file($file_tmp_loc, $file_store);

  $update="UPDATE posts SET title='$title',description='$description',image='$file_name',category_id='$categories' WHERE id=".$id;
  $query=$con->query($update);
  if ($query) {
    echo "success";
    //header('location:.php');
  }else{
    echo 'not success';
  }
}
?>

<div style="">
    <?php include 'pages/footer.php'; ?>
</div>